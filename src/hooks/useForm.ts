import { useState } from "react";

export const useForm =  <T extends object>(estadoInicial: T ) => {
    const [formulario, setFormulario] =useState(estadoInicial)
    
    const onChange  = <K extends object>(campo:keyof T, valor:keyof K) => {
        setFormulario(
          {
            ...formulario, [campo]:valor
          }
        )
    }
    const resetFormulario = () =>{
        setFormulario(estadoInicial)
    }
    return{
        ...formulario, formulario,onChange,resetFormulario
    }

}