import { useNavigate } from "react-router-dom"
import { BotonVolver } from "../components/BotonVolver"
import { Categoria } from "../interfaces/Categoria"
import { useState } from "react"
import { useForm } from "../hooks/useForm"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPlus } from "@fortawesome/free-solid-svg-icons"


const estadoInicial = {nombre:'', descripcion:''}

export const CategoriasPages = () => {
  const [categorias, setCategorias] = useState<Categoria[]>([])
  const {nombre,descripcion, onChange,resetFormulario} = useForm(estadoInicial)
  const navigate = useNavigate()

  const navergarAProductos = () =>{
      navigate('/productos')
  }

  const agregarCategoria = (nombre: string, descripcion: string) => {
    
    const nuevaCategoria = {
      id: categorias.length +1,
      nombre,
      descripcion, 
    }
    setCategorias(categorias.concat(nuevaCategoria))
  }

  const onAgregarClicked = () => {
    if(descripcion.length == 0 || nombre.length == 0) {
      alert("Ingrese todos los campos")
      return
    }

      agregarCategoria (nombre, descripcion)
    
      resetFormulario()
  }
  
  return (
    <>
      <div style={{ display: "flex", alignItems: "flex-end" }}>
        <BotonVolver />
        <h1 style={{ margin: 0, marginLeft: 10 }}>Categorías</h1>
      </div>
      <hr />
      <div className="container-fluid "> 
        <div className="input-group mb-3">
        <input className="input-group-text me-2" type="text" name="nombre" placeholder="Nombre"
        value={nombre} onChange={(event)=> onChange('nombre',event.target.value)
        }
        />
        <input className="input-group-text me-2" type="text" name="descripcion" placeholder="Descripción"
        value={descripcion} onChange={(event)=>onChange('descripcion', event.target.value)}
        />
        </div>
        <button className="btn btn-outline-secondary" onClick={onAgregarClicked}>
           <FontAwesomeIcon icon={faPlus}  />
            Agregar Categoría
        </button>

        <button className="btn btn-outline-secondary mx-2" onClick={navergarAProductos}>Productos</button>
          <table className="table table-bordered border-secondary mt-2">
            <thead>
              <tr>
                <th scope="col" className="text-center">ID</th>
                <th scope="col" className="text-center">Nombre</th>
                <th scope="col" className="text-center">Descripción</th>
              </tr>
            </thead>
            <tbody>
            {
              categorias.map(categoria => (
                <tr key={categoria.id}>
                <td className="text-center">{categoria.id}</td>
                <td className="text-center">{categoria.nombre}</td>
                <td className="text-center">{categoria.descripcion}</td>
                </tr>
              ))
            }
            </tbody>
          </table>
      </div>
    </>
  )
}
