import { useState } from "react"
import { BotonVolver } from "../components/BotonVolver"
import { Usuario } from "../interfaces/Usuario"
import { useForm } from "../hooks/useForm"

const estadoInicial = {nombre:'', apellido:''}

export const UsuariosPages = () => {
  const [usuarios, setUsuarios] = useState<Usuario[]>([])
  const {nombre, apellido, onChange,resetFormulario} = useForm(estadoInicial)
  


  const agregarUsuario = (nombre: string, apellido: string) => {
    const nuevoUsuario = {
      id: usuarios.length +1,
      nombre, 
      apellido
    }
    setUsuarios(usuarios.concat(nuevoUsuario))
    
  }
  const onAgregarClicked = () => {
    if(nombre.length == 0 || apellido.length == 0) {
      alert("Ingrese todos los campos")
      return
    }

    agregarUsuario (nombre,apellido)
    resetFormulario()
  }

  return (
    <>
      <div style={{ display: "flex", alignItems: "flex-end" }}>
        <BotonVolver />
        <h1 style={{ margin: 0, marginLeft: 10 }}>Usuario</h1>
      </div>
      <hr />
      <ul>
        {
          usuarios.map(usuario =>  <li key={usuario.id}>{usuario.nombre} {usuario.apellido}</li>)
        }
      </ul>
      
      <div className="input-group mb-3">
        <input className="input-group-text me-2" type="text" name="nombre" placeholder="Nombre"
        value={nombre} onChange={(event)=> onChange('nombre',event.target.value)}
        />
        <input className="input-group-text me-2" type="text" name="apellido" placeholder="Apellido"
        value={apellido} onChange={(event)=>onChange('apellido',event.target.value)}
        />
      </div>
      
      <button className="btn btn-outline-secondary" onClick={onAgregarClicked}> 
        Agregar Usuario
      </button>
    </>
  )
}
