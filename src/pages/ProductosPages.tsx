import { useState } from "react"
import { BotonVolver } from "../components/BotonVolver"
import { Producto } from "../interfaces/Producto"
import { useForm } from "../hooks/useForm"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPlus } from "@fortawesome/free-solid-svg-icons"

const estadoInicial = {descripcion:'', precio:''}

export const ProductosPages = () => {
  const [productos, setProductos] = useState<Producto[]>([])
  const {descripcion,precio, onChange,resetFormulario} = useForm(estadoInicial)


  const agregarProducto = (descripcion: string, precio: string) => {
    const nuevoProducto = {
      id: productos.length +1,
      descripcion, //equivale a descripcion: descripcion,
      precio
    }
    setProductos(productos.concat(nuevoProducto))
    
  }
  
  
  const onAgregarClicked = () => {
    if(descripcion.length == 0 || precio.length == 0) {
      alert("Ingrese todos los campos")
      return
    }

      agregarProducto (descripcion,precio)
    
      resetFormulario()
  }

  return (
    <>
      <div style={{ display: "flex", alignItems: "flex-end" }}>
        <BotonVolver />
        <h1 style={{ margin: 0, marginLeft: 10 }}>Produto</h1>
      </div>
      <hr />
      <div className="container-fluid "> 
        <div className="input-group mb-3">
        <input className="input-group-text me-2" type="text" name="descripcion" placeholder="Descripción"
        value={descripcion} onChange={(event)=> onChange('descripcion',event.target.value)
        }
        />
        <input className="input-group-text me-2" type="text" name="precio" placeholder="Precio"
        value={precio} onChange={(event)=>onChange('precio', event.target.value)}
        />
        </div>
        <button className="btn btn-outline-secondary" onClick={onAgregarClicked}>
           <FontAwesomeIcon icon={faPlus}  />
            Agregar Producto
        </button>

          <table className="table table-bordered border-secondary mt-2">
            <thead>
              <tr>
                <th scope="col" className="text-center">Descripción</th>
                <th scope="col" className="text-center">Precio</th>
              </tr>
            </thead>
            <tbody>
            {
              productos.map(producto => (
                <tr key={producto.id}>
                <td className="text-center">{producto.descripcion}</td>
                <td style={{ textAlign: "right" }}>{producto.precio}</td>
                </tr>
              ))
            }
            </tbody>
          </table>
        </div>
    </>
  )
}
