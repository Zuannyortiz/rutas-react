import { BotonNavegador } from "../components/BotonNavegador"

export const InicioPages = () => {

  return (
    <>
      <h1>Inicio</h1>
      <hr />
      <BotonNavegador name="Producto" ruta="/productos" margenDerecha/>
      <BotonNavegador name="Usuario" ruta="/usuarios" margenDerecha/>
      <BotonNavegador name="Categoría" ruta="/categorias" margenDerecha/>
    </>
  )
}
