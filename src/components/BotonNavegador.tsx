import { useNavigate } from 'react-router-dom'

interface Props {
    name: string,
    ruta: string,
    margenDerecha?:boolean,
}
export const BotonNavegador = ({name,ruta, margenDerecha = false}:Props) => {

    const navigate = useNavigate()

    const navergarADestino = () =>{
        navigate(ruta)
    }
  return (
    <button className="btn btn-secondary" onClick={navergarADestino}style={{marginRight: margenDerecha ? 5 : undefined}}>{name}</button>
  )
}
