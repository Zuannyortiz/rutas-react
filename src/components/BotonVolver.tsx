import React from "react";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";

export const BotonVolver = () => {
    const navigate = useNavigate();
    
    return (
        <div>
            <button className="btn btn-outline-danger" style={{ marginTop: 30 }} onClick={() => navigate(-1)}>
                <FontAwesomeIcon icon={faArrowLeft} />
            </button>
        </div>
    );
};
