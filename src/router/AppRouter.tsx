import {Navigate, Route, Routes} from "react-router-dom"
import { InicioPages } from "../pages/InicioPages"
import { ProductosPages } from "../pages/ProductosPages"
import { UsuariosPages } from "../pages/UsuariosPages"
import { CategoriasPages } from "../pages/CategoriasPages"
export const AppRouter = () => {
  return (
    <Routes>
        <Route path="/*" element={<Navigate to='/inicio'/>} />
        <Route path="inicio" element={<InicioPages/>} />
        <Route path="productos" element={<ProductosPages/>} />
        <Route path="usuarios" element={<UsuariosPages/>} />
        <Route path="categorias" element={<CategoriasPages/>} />
    </Routes>
  )
}
